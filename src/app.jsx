import { LocationProvider, Router, Route } from "preact-iso";

import Home from "./routes/Home";
import NotFound from "./routes/NotFound";

export default function App() {
  return (
    <LocationProvider>
      <Router>
        <Route component={Home} path="/" />
        <Route component={NotFound} default />
      </Router>
    </LocationProvider>
  );
}
