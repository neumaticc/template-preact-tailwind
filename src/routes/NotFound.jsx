export default function NotFound() {
  return (
    <>
      <h1 className="text-2xl font-semibold">Page not found</h1>
      <h2 className="text-lg">Page not found</h2>
    </>
  );
}
