import { useState } from "preact/hooks";

export default function Home() {
  const [count, setCount] = useState(0);
  return (
    <>
      <h1>Vite + Preact + Tailwind + ESLint</h1>
      <button
        type="button"
        onClick={() => setCount((c) => c + 1)}
        className="px-4 py-2 border-2 border-black/50 rounded-full"
      >
        count is
        {" "}
        {count}
      </button>
    </>
  );
}
