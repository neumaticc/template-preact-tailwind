# template-preact-tailwind

Uses [Preact](https://preactjs.com) with [Vite](https//vitejs.dev) and [Tailwind](https://tailwindcss.com) for styling. Additionally, it has [ESLint](https://eslint.org) with [React](https://github.com/jsx-eslint/eslint-plugin-react) and [AirBnB](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb) rules, as well as some [custom ones/overrides]

## Get started
```sh
curl https://gitlab.com/neumaticc/template-preact-tailwind/-/raw/main/dl.sh?ref_type=heads | sh
```

## Additional features
- [Preact isomorphic](https://github.com/preactjs/preact-iso) routing
- Uses [Inter](https://github.com/rsms/inter) font
- Environment variables
- Public and Assets directories